/////////////////////////////////////////////     1    /////////////////////////////////////////////////
function isUndefined(obj) {
    return obj === undefined ? true : false;
}

function isNull(obj) {
    return obj === null ? true : false;
}

function isBoolean(obj) {
    return typeof (obj) === "boolean" ? true : false;
}

function isNumber(obj) {
    return typeof (obj) === "number" ? true : false;
}

function isString(obj) {
    return typeof (obj) === "string" ? true : false;
}

function isFunction(obj) {
    return typeof (obj) === "function" ? true : false;
}

function isArray(obj) {
    return Array.isArray(obj);
}

function isObject(obj) {
    return typeof (obj) === "object" && !Array.isArray(obj) ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////         2       ///////////////////////////////////////////////////////

function equals(a, b) {

    return typeof (a) === typeof (b) && Object.valueOf(a) === Object.valueOf(b);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////          3        //////////////////////////////////////////////////////

function copy(obj) { //All types, excipere Symbol types
    let result;
    if (isObject(obj) || isArray(obj)) {
        result = new Object(obj);
    } else if (isString(obj)) {
        result = "" + obj
    } else if (isNumber(obj)) {
        result = +obj;
    } else if (isBoolean(obj)) {
        result = obj ? true : false;
    } else result = obj; // if obj = Symbol
    return result;
}

function copyObjectsAndArrays(obj) { // ONLY OBJECTS AND ARRAYS
    let result;
    if (isArray(obj)) {
        result = [];
        obj.forEach((item) => result.push(item));
    } else if (isObject(obj)) {
        result = {};
        Object.assign(result, obj)
    }
    return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////            4           ////////////////////////////////////////////////////

function objBust(obj, key,tab) {
    if (isUndefined(key)) {
        key = "Object:";
    }
    document.write(key+":<br>");
    if (isObject(obj)) {
        for (let entries of Object.entries(obj)) {
            let ent_key = entries[0]
            let ent_value = entries[1]
            document.write(key+ent_key+":");
            if (isObject(ent_value)) {
                document.write(ent_value + " - <b>OBJECT</b><br>");
                objBust(ent_value, key+"\\"+ent_key+"\\")
            } else {
                document.write(ent_value+"<br>");
            }
        }
    }
}

// Вывод инфы на страницу
objBust({
    id: "1",
    name: "Garold",
    "Friends": {
        "mans": {
            id: 15,
            name: "petya"
        },
        "womans": {
            id: 19,
            name: "Olga",
        },
    },

});
